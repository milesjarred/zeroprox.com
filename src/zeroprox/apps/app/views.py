from django.views.generic import TemplateView

from . import models

# Create your views here.


class HomeView(TemplateView):
    template_name = 'app/../templates/cms_templates/home.html'

    def get_context_data(self, **kwargs):
        ctx = super(HomeView, self).get_context_data(**kwargs)
        ctx['skills'] = self.get_skills()
        ctx['work_exps'] = self.get_work_exps()
        ctx['educations'] = self.get_educations()
        ctx['coding_music'] = self.get_coding_music()
        return ctx

    def get_skills(self):
        return models.Skill.objects.prefetch_related('skill_rank').all().order_by('order')[:10]

    def get_work_exps(self):
        return models.WorkExperience.objects.all().order_by('start_date')[:10]

    def get_educations(self):
        return models.Education.objects.all()[:10]

    def get_coding_music(self):
        return models.CodingMusic.objects.all()[:10]
