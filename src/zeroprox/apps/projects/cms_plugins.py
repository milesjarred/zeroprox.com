from cms.plugin_base import CMSPluginBase
from cms.models.pluginmodel import CMSPlugin
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _


class LatestProjectsPlugin(CMSPluginBase):
    model = CMSPlugin
    name = _("Latest Projects")
    module = _("Projects")
    render_template = 'projects/latest_projects.html'
    cache = False


class OtherProjectsPlugin(CMSPluginBase):
    model = CMSPlugin
    name = _("Other Projects")
    module = _("Projects")
    render_template = 'projects/other_projects.html'
    cache = False

plugin_pool.register_plugin(LatestProjectsPlugin)
plugin_pool.register_plugin(OtherProjectsPlugin)
