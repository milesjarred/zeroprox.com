from cms.models.pluginmodel import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from . import models


class SkillsPlugin(CMSPluginBase):
    model = CMSPlugin
    name = _("Skills Plugin")
    render_template = 'app/modules/skills.html'
    cache = False
    module = _("Side Bar Plugins")

    def render(self, context, instance, placeholder):
        ctx = super(SkillsPlugin, self).render(context, instance, placeholder)
        ctx['skills'] = models.Skill.objects.prefetch_related('skill_rank').all().order_by('order')[:10]
        return ctx


class EducationPlugin(CMSPluginBase):
    model = CMSPlugin
    name = _("Education Plugin")
    render_template = 'app/modules/education.html'
    cache = False
    module = _("Side Bar Plugins")

    def render(self, context, instance, placeholder):
        ctx = super(EducationPlugin, self).render(context, instance, placeholder)
        ctx['educations'] = models.Education.objects.all()[:10]
        return ctx


class CodingMusicPlugin(CMSPluginBase):
    model = CMSPlugin
    name = _("Coding Music Plugin")
    render_template = 'app/modules/music.html'
    cache = False
    module = _("Side Bar Plugins")

    def render(self, context, instance, placeholder):
        ctx = super(CodingMusicPlugin, self).render(context, instance, placeholder)
        ctx['coding_music'] = models.CodingMusic.objects.all().order_by('name')[:10]
        return ctx


class LanguagesPlugin(CMSPluginBase):
    model = CMSPlugin
    name = _("Languages Plugin")
    render_template = 'app/modules/languages.html'
    cache = False
    module = _("Side Bar Plugins")


class AboutMePlugin(CMSPluginBase):
    model = models.AboutMe
    name = _("About Me Plugin")
    render_template = 'app/modules/about_me.html'
    cache = False
    module = _("Main Plugins")


plugin_pool.register_plugin(SkillsPlugin)
plugin_pool.register_plugin(EducationPlugin)
plugin_pool.register_plugin(CodingMusicPlugin)
plugin_pool.register_plugin(LanguagesPlugin)
plugin_pool.register_plugin(AboutMePlugin)
