# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-29 05:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20170329_0003'),
    ]

    operations = [
        migrations.AddField(
            model_name='workexperience',
            name='employer_link',
            field=models.URLField(blank=True),
        ),
    ]
