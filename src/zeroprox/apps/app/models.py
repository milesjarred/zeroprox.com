from django.db import models
from django.contrib.auth.models import User

from cms.models.pluginmodel import CMSPlugin
from djangocms_text_ckeditor.fields import HTMLField

# Create your models here.


class SkillRank(CMSPlugin):
    """
    Skill rank I.E (Novice)
    """
    name = models.CharField(max_length=50)

    def copy_relations(self, old_instance):
        self.associated_item.all().delete()

        for item in old_instance.associated_item.all():
            item.pk = None
            item.skill_rank = self
            item.save()

    def __str__(self):
        return self.name


class Skill(models.Model):
    """
    A skill I.E (Html)
    """
    name = models.CharField(max_length=144)
    percent = models.IntegerField()
    skill_rank = models.ForeignKey(SkillRank)
    tool_tip = models.CharField(max_length=144, default=None, null=True, blank=True)
    order = models.IntegerField(default=1000)

    def __str__(self):
        return self.name


class WorkExperience(models.Model):
    """
    A model for Work Experiences
    """
    job_title = models.CharField(max_length=144)
    employer_name = models.CharField(max_length=144)
    employer_link = models.URLField(blank=True)
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    description = models.TextField()

    def __str__(self):
        return '{} at {}'.format(self.job_title, self.employer_name)


class Education(models.Model):
    """
    A model for Education
    """
    degree = models.CharField(max_length=144)
    college_name = models.CharField(max_length=144)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return '{} from {}'.format(self.degree, self.college_name)


class CodingMusic(models.Model):
    """
    A model for coding music
    """
    name = models.CharField(max_length=144)
    link = models.URLField()

    class Meta:
        verbose_name = 'Coding Music'

    def __str__(self):
        return self.name


class AboutMe(CMSPlugin):
    """
    Model for storing a bout me items for the about me plugin
    """
    description = HTMLField(blank=True)
